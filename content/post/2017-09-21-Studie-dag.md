---
Title: Studie dag. 
Date: 2017-09-21 
---
We hadden vandaag geen werkcollege, aangezien de docenten een studie dag hadden. Mijn *tools for design* ging wel door. We kregen daar de basis van InDesign en hoe je een bestand opent. Volgende week beginnen we daar aan onze eerste opdracht. 