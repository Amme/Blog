---
Title: Spelanalyse. 
Date: 2017-10-02
---

Vandaag heb ik weer een planning gemaakt met wat we nog af moeten maken en wie dat gaat doen.  Ons spel laten testen stond hoog op de planning en daar gingen we dan ook meteen mee aan de slag.
Coen Ooms (team Forck) was ons test-kandidaat.  
Ik hielt bij wat zijn emoties waren tijdens het spel en heb uiteindelijk het spelanalyse geschreven. Wat voornamelijk uit de test kwam was dat hij het nogal onduidelijk vond. Verder vond hij het spel qua concept en uiterlijk wel erg leuk bedacht en passend bij onze doelgroep.