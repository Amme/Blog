---
Title: Nieuw concept. 
Date: 2017-09-25
---

Vandaag gingen we aan de slag met het bedenken van nieuwe concepten.
We begonnen eerst met het maken van een ontwerpproceskaart (nog over iteratie 1).  
We hebben een brainstorm sessie gedaan over nieuwe concept ideeën. Ieder schreef een idee van een spel vorm op een post-IT die wij daarna op het raam plakte. Hierna plaatste wij die op een A3 met de (niet) realistische gewoon en ongewoon vakken. Na wat overleg kwamen er 3 goede concepten uit. Waarvan wij de voor en nadelen opschreven. We zijn er nog niet helemaal uit welk concept het wordt, maar daar gaan wij woensdag weer over praten. 

*Feedback Mio*: Beschrijf de criteria die nodig is voor het spel bij het kiezen van een nieuw concept. 