---
Title: Concept. 
Date: 2017-09-11
---

We begonnen de dag door met elkaar door te nemen wat wij deze komende week gaan doen. We besloten een brainstorm te houden over het concept van het spel. 
***Het spel idee***: De basis is een tour door Rotterdam, in groepjes door middel van een app die je rondleid naar nieuwe locaties. Bij die locaties (kunst) vind de speler QR codes die hij/zij moet scannen en zo vragen krijgt via die app. Als het groepje moet je deze vraag beantwoorden. Is deze goed dan verschijnen er letters  onderin het beeld die na alle vragen goed beantwoord te hebben een zin vormen. 
Om het spel wat meer interactief te maken hebben wij later bedacht om spelkaarten toe te voegen. Het idee erachter is dat je met deze kaarten andere groepjes kan pesten door bijvoorbeeld pestkaarten (ga 2 vragen terug), maar ook kaarten die jou groepje vooruit helpen.

*Feedback*: We moeten nog goed gaan bedenken hoe we dit precies gaan uitwerken en dat alles duidelijk wordt voor de spelers.

Verder die dag heb ik nog een planning gemaakt voor de komende 4 weken. Met wat er allemaal af moet en wie deze taken gedaan hebben.

De workshop blog maken vond ik erg lastig en het ging ook allemaal niet zo als ik wilde. De stappen waren duidelijk, maar toch kreeg ik het voor elkaar om het allemaal door elkaar te doen en uiteindelijk liep ik vast. Gelukkig kwam Mio mij helpen en is het toch nog gelukt om mijn blog online te krijgen.
- Ik vond het persoonlijk niet echt leuk om te doen, maar het lijkt mij wel interessant om meer over te weten en beter in te worden.