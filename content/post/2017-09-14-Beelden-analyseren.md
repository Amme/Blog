---
Title: Beelden analyseren.  
Date: 2017-09-14
---

Als eerst had ik in de ochtend de personal challenge InDesign, maar helaas had de docent zich verslapen en verviel deze les. Ik heb toen wat voor mijzelf gedaan (visueel onderzoek).


**Werkcollege 2.**
We moesten 5 afbeeldingen meenemen, met een retorische functie. Tijdens de les moesten wij deze 5 afbeeldingen uit knippen en op een apart A4'tje plakken. Daarna moesten wij 4 vragen over de afbeelding beantwoorden.
*Feedback docent*: Het helpt om in plaats van alleen maar tekst door symbooltjes erbij te tekenen, dat maakt het in één oog opslag duidelijk.

