---
Title: Kill your darlings.
Date: 2017-09-20
---

Maandag kwam het nieuws dat ons concept de prullenbak in kon. Vandaag kregen wij de nieuwe uitleg van iteratie 2 en de deliverables. Daar hebben we een planning en taakverdeling van gemaakt. 
Zoya & ik zijn toen begonnen met het kijken naar de doelgroep Art & Crafts. We gingen aan de slag met een nieuw interview die dieper in gaat op de uitkomsten van ons vorige doelgroeponderzoek. 
*Feedback Jantiene*: Kijk goed naar het vorige concept, kan je elke keuze beargumenteren met  onderzoekresultaten. Zo niet ga dat dan nu onderzoeken of het echt wel zo is.
bijv.: Jullie kozen voor kennis vragen, maar houdt de doelgroep wel van kennis vragen?
We hebben de vragen bedacht en uitgetypt. Morgen gaan we naar WdKa om daar het interview te houden.

**Workshop.**
Deze keer heb ik gekozen voor *Creatieve Technieken.* Hier gingen we leren Brainstormen en wat dat inhoudt.  In een groepje van 5 kreeg je een probleem. Het probleem was: *Honden kunnen met mensen praten welk product ga jij op de markt brengen dat voor beide (hond en mens) te gebruiken is.*  We begonnen met het uitkiezen van een voorzitter en een notulist (dat was ik). Daarna gingen we aan de slag met allemaal verschillende dingen te zeggen die eventueel als product gemaakt kunnen worden, elk idee telt ook hele slechte of niet denkbare. Na een half uurtje was het tijd om de ideeën in te delen in 4 vakken. realistisch en gewoon - realistisch en speciaal - nog niet realistisch en gewoon - nog niet realistisch en speciaal.
Op het einde moest in elk vak maar één idee overblijven en daarna één idee, wat uiteindelijk het idee werd. Bij ons was dat: een telefoon met spraakerkenning. 
*Feedback docent*:  Jullie hadden in het begin veel ideeën, dat is goed. Maar daarna bij het plaatsen van de ideeën en het uitzoeken ging het wat moeizaam. 

**Studie coaching.**
Tijdens SC moesten wij tussen 2 personen in gaan zitten waarvan je de naam niet wist. Dit zijn er voor mij al best veel aangezien ik niet goed ben met namen onthouden en vele van mijn klas is ook nooit echt heb gesproken, omdat iedereen zijn of haar eigen groepje heeft van kamp of het project. 
We zaten in groepjes van 5 en moesten per persoon de typische CMD'er tekenen met kernwoorden erbij. Daarna bespraken wij met z'n 5e waar iedereen op uit kwam. 
Ook bespraken we de 3 competenties wat ze inhouden. 
Op het laatste moest 1 iemand van het groepje overgetrokken worden op een groot vel papier. En daarbij moest je de kernwoorden van de CMD'er schrijven. 
Het einde van de les kregen wij per projectgroepje onze feedback van de SWOT. 
*Feedback*: Onvoldoende kwaliteit.
jullie missen de vooropleidingen en werkervaring. Teamafspraken zijn wel voldoende, maar meer specifieker. De SWOT: dreigingen niet goed geformuleerd, koppelingen oppervlakkig, samenhang minimaal.
