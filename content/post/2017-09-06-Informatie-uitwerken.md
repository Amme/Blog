---
Title: Informatie uitwerken.  
Date: 2017-09-06
---

Vandaag gingen Zoya, Elien & ik de resultaten uitwerken. Daarna heb ik kort een toelichting getypt bij elke uitslag en waarom we die vraag gesteld hadden. 
*Feedback*:
**Mio**: Het zou leuker zijn als je de resultaten op een creatievere manier uitwerkt, door middel van visualisatie.

Met deze feedback zijn wij verder gegaan en hebben Zoya & ik een (tekst) bedacht. De verschillende icoontjes uitgekocht en op illustrator uitgewerkt.

In de middag kregen wij studie coaching. Dit was erg interessant en leuk. 
En workshop blog maken. Ik had niet echt verwacht dat we hem ook zelf moesten gaan maken met Markdown. Maar daar ben ik wel benieuwd naar hoe dat allemaal werkt. 
