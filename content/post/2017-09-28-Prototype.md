---
Title: Prototype. 
Date: 2017-09-28
---

Zoya & ik bleven na ons werkcollege lang op school om te werken aan het paper prototype. De rest van ons team was helaas naar huis gegaan.
Het idee was om het op Ipad formaat te maken. En die sneden we uit witte foam. Daarna hebben we A4'tjes op Ipadscherm maat uitgeknipt en hier onze game pagina's op uit getekend. 
Deze papiertjes hebben wij aan het foam vast gemaakt door middel van 2 kleine ringbandjes boven aan. Zo krijg je een leuk omslag effect.
We zijn hier 3 uur mee bezig geweest.