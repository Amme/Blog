---
Title: Presentatie iteratie 2. 
Date: 2017-10-04
---

Het was de dag van de presentaties. Wij waren in de ochtend bij Mio, een alumni en een andere docent van CMD. 
*Feedback*: 
- De presentatie was goed, het concept is goed bedacht en past bij de doelgroep en voldoet aan de eisen van de klant. 
- Verder moeten we alleen nog even kijk naar de deuren in het spel, in hoe verre die belangrijk zijn voor het spel / doelgroep. 
- Ook was de speluitleg nog te lang om in 5 min te begrijpen.