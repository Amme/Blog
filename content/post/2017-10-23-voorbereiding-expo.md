---
title: voorbereiding expo
date: 2017-10-23
---

Dit is de laatste dag, woensdag is de EXPO waarop wij ons spel gaan presenteren aan de docenten en medestudenten. We hebben besproken hoe onze stand eruit komt te zien, na verschillende opties kwamen we uit bij een simpele tafel. Met alleen een groot horizontaal foambord met daarop ons proces uitgetekend, in een vorm van een schatkaart. Die uiteindelijk uitkomt bij de Ipad, waarop je het spel kan spelen.
Ik heb ook verder gewerkt aan mijn Leerdossier.