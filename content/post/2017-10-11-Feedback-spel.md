---
Title: Feedback spel. 
Date: 2017-10-11 
---

Elien, Zoya & ik hebben feedback gevraagd aan Mio. We hebben hem via de Ipad het spel laten spelen.  
*Feedback Mio*: Het spel ziet er netjes uit voor een prototype.
De nieuwe aanpassingen na de presentatie, zoals het verwijderen van de deuren zijn een goede keuze geweest. Wel is het voor mij een beetje onduidelijk hoe je nu met de docenten/peercoachen moet chatten. Dus misschien beter uitleggen in het begin.

Na deze feedback hebben wij het "help" knopje aangepast.