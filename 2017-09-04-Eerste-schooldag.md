---
Title:De eerste schooldag  
Date: 2017-09-04
Opdracht: _Ontwerp een spel dat gebruikt maakt van een online interactief element dat in de introductieweek van 2018 studenten in Rotterdam met elkaar en de stad verbindt._

We begonnen de dag met het bespreken van de opdracht en het plannen van de taken. Ook hebben wij deze dag al de doelgroep  bepaald & een interview samengesteld die wij gaan afnemen bij de studenten zodat we hieruit een thema kunnen kiezen.

_Taken_:
Wat voor soort games zijn er.
Doelgroep bedenken. (Arts & Crafts op WdKa).
Interview / enquête vragen bedenken & uitwerken.

_Feedback_: 
**Mio**: Welke methode werkt het beste voor ons interview een enquête  of misschien wel beide. En wat voor vragen ga je dan stellen? 
Denk niet te snel aan het eindproduct, doe goed onderzoeken.
