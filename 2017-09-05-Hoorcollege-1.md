---
Title: Hoorcollege 1.
Date: 2017-09-05
Het eerste hoorcollege ging vooral over het *ontwerpproces*. 
Ik vond de informatie erg nuttig en leerzaam. 

Later die dag gingen ik, samen met Zoya & Elien naar de Willem de Kooning academie om daar ons interview & enquête af te nemen. We vonden al snel een groep die Arts & Crafts deden en zij vonden het niet erg om even tijd vrij te maken voor ons. 
Waarom wij uiteindelijk gekozen hebben om beide af te nemen was vooral omdat wij graag snelle informatie wilde verzamelen door middel van een enquête, maar daarnaast ook wat diepere vragen wilde stellen en eventueel door wilde vragen. En daarom kozen wij er ook voor  een interview ernaast te doen.